const path = require("path");

module.exports = {
  i18n: {
    defaultLocale: "ja",
    locales: ["ja"],
    // localeDetection: false,
  },
  react: { useSuspense: false },
  localePath: path.resolve("./locales"),
};
