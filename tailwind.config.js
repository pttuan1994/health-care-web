/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        background: "#F2F2F2",
        light: "#FFFFFF",
        primary300: "#FFCC21",
        primary400: "#FF963C",
        primary500: "#EA6C00",
        primaryLinear: "linear-gradient(180deg, #FFCC21 0%, #FF963C 100%)",
        secondary300: "#8FE9D0",
        dark500: "#414141",
        dark600: "#2E2E2E",
        gray300: "#707070",
        gray400: "#777777",
      },
      fontFamily: {
        inter: ["Inter"],
      },
    },
  },
  plugins: [
    ({ addComponents, theme }) => {
      addComponents({
        ".container": {
          "@apply mx-auto": {},
          "@apply px-4": {},
          "@apply min-[1280px]:max-w-[1024px]": {},
        },
      });
    },
  ],
};
