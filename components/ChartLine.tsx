import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Line } from "react-chartjs-2";
import { theme as themeTailwind } from "../tailwind.config";

const theme: any = themeTailwind.extend;

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

const options = {
  responsive: true,
  maintainAspectRatio: false,
  scales: {
    y: {
      grid: {
        display: false,
      },
      ticks: {
        display: false,
      },
    },
    x: {
      grid: {
        color: theme.colors.gray400,
      },
      ticks: {
        color: theme.colors.light,
      },
    },
  },
  plugins: {
    legend: {
      display: false,
    },
  },
};

const labels = [
  "6月",
  "7月",
  "8月",
  "9月",
  "10月",
  "11月",
  "12月",
  "1月",
  "2月",
  "3月",
  "4月",
  "5月",
];

const ChartLine = ({ data, width, height }) => {
  return (
    <div style={{ width, height }}>
      <Line
        options={options}
        data={{
          labels,
          datasets: data.map((item, index) => {
            const color =
              index === 0 ? theme.colors.primary300 : theme.colors.secondary300;
            return {
              data: item,
              borderColor: color,
              backgroundColor: color,
            };
          }),
        }}
      />
    </div>
  );
};

export default ChartLine;
