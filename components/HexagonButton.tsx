import styled from "@emotion/styled";

const HexagonShape = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  margin: 0 auto;
  background: ${({ theme }: any) =>
    `linear-gradient(155.89deg, ${theme.colors.primary300} 8.26%, ${theme.colors.primary400} 91.18%)`};
  width: 134px;
  height: 77px;
  box-sizing: border-box;
  transition: all 1s;
  transform: translate(-50%, -50%);

  /* Creating pseudo-class */
  &:before,
  &:after {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
  }

  /* Align them in such a way
        that they form a hexagon */
  &:before {
    transform: rotate(60deg);
    background: ${({ theme }: any) =>
      `linear-gradient(95.89deg, ${theme.colors.primary300} 8.26%, ${theme.colors.primary400} 91.18%)`};
  }
  &:after {
    transform: rotate(-60deg);
    background: ${({ theme }: any) =>
      `linear-gradient(215.89deg, ${theme.colors.primary300} 8.26%, ${theme.colors.primary400} 91.18%)`};
  }
`;

const HexagonText = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
`;

const HexagonButton = ({ text, icon, onClick }) => {
  return (
    <div
      className="relative w-[134px] h-[134px] cursor-pointer hover:opacity-70"
      onClick={onClick}
    >
      <HexagonShape></HexagonShape>
      <HexagonText>
        <img width={56} height={56} src={icon} alt="icon button" />
        <p className="font-inter text-xl text-light">{text}</p>
      </HexagonText>
    </div>
  );
};

export default HexagonButton;
