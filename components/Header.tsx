import { useRouter } from "next/router";
import Link from "next/link";
import { useState, useRef } from "react";
import styled from "@emotion/styled";

const WrapperHeader = styled.div`
  z-index: 10;
  background-color: ${({ theme }: any) => theme.colors.dark500};
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.160784);
`;

const Header = () => {
  const { pathname } = useRouter();
  const buttonMenuRef = useRef<any>();
  const [isOpenMenu, setIsOpenMenu] = useState(false);
  const [isLeaveMenu, setIsLeaveMenu] = useState(true);

  return (
    <>
      <WrapperHeader>
        <nav className="container flex items-center justify-between">
          <Link href="/" className="w-[144px]">
            <img
              className="object-cover w-full h-full"
              src="/logo.png"
              alt="main logo"
            ></img>
          </Link>
          <ul className="flex items-center justify-left  space-x-8">
            <li className="flex items-center justify-left space-x-2">
              <div className="w-[32px] h-[32px]">
                <img
                  className="object-cover w-full h-full"
                  src="/icons/icon_memo.png"
                  alt="navigate icon"
                ></img>
              </div>
              <Link
                className={`font-light hover:text-primary400 ${
                  pathname === "/record" ? "text-primary400" : "text-light"
                }`}
                href="/record"
              >
                自分の記録
              </Link>
            </li>
            <li className="flex items-center justify-left space-x-2">
              <div className="w-[32px] h-[32px]">
                <img
                  className="object-cover w-full h-full"
                  src="/icons/icon_challenge.png"
                  alt="navigate icon"
                ></img>
              </div>
              <Link
                className="text-light font-light hover:text-primary400"
                href="/"
              >
                チャレンジ
              </Link>
            </li>
            <li className="flex items-center justify-left space-x-2">
              <div className="relative w-[32px] h-[32px]">
                <img
                  className="object-cover w-full h-full"
                  src="/icons/icon_info.png"
                  alt="navigate icon"
                ></img>
                <p className="flex items-center justify-center absolute top-0 -right-2 w-[16px] h-[16px] bg-primary500 text-light text-[10px] rounded-full">
                  1
                </p>
              </div>
              <Link
                className="text-light font-light hover:text-primary400"
                href="/"
              >
                お知らせ
              </Link>
            </li>
            <li className="flex items-center justify-left space-x-2">
              <div className="relative w-[32px] h-[32px]">
                <img
                  className="object-cover w-full h-full"
                  src={
                    isOpenMenu
                      ? "/icons/icon_close.png"
                      : "/icons/icon_menu.png"
                  }
                  alt="navigate icon"
                ></img>
                <input
                  ref={buttonMenuRef}
                  type="button"
                  className="absolute top-0 left-0 w-[32px] h-[32px] cursor-pointer"
                  onClick={() => {
                    setIsOpenMenu(!isOpenMenu);
                  }}
                  onBlur={() => {
                    isLeaveMenu && setIsOpenMenu(!isOpenMenu);
                  }}
                />
              </div>
            </li>
          </ul>
        </nav>
      </WrapperHeader>
      {isOpenMenu ? (
        <div className="z-10 fixed top-[64px] right-0 w-full">
          <nav className="container flex items-center justify-end">
            <ul
              className="bg-gray400 min-w-[280px]"
              onMouseEnter={() => {
                setIsLeaveMenu(false);
              }}
              onMouseLeave={() => {
                buttonMenuRef.current.focus();
                setIsLeaveMenu(true);
              }}
              onClick={() => {
                setIsLeaveMenu(true);
                setIsOpenMenu(false);
              }}
            >
              <li className="relative px-[32px] py-[23px]">
                <Link
                  href="/record"
                  className="text-light font-light text-lg hover:opacity-70"
                >
                  自分の記録
                </Link>
                <div className="w-full absolute left-0 bottom-0 bg-light h-px opacity-[.15]"></div>
              </li>
              <li className="relative px-[32px] py-[23px]">
                <Link
                  href="/"
                  className="text-light font-light text-lg hover:opacity-70"
                >
                  体重グラフ
                </Link>
                <div className="w-full absolute left-0 bottom-0 bg-light h-px opacity-[.15]"></div>
              </li>
              <li className="relative px-[32px] py-[23px]">
                <Link
                  href="/"
                  className="text-light font-light text-lg hover:opacity-70"
                >
                  目標
                </Link>
                <div className="w-full absolute left-0 bottom-0 bg-light h-px opacity-[.15]"></div>
              </li>
              <li className="relative px-[32px] py-[23px]">
                <Link
                  href="/"
                  className="text-light font-light text-lg hover:opacity-70"
                >
                  選択中のコース
                </Link>
                <div className="w-full absolute left-0 bottom-0 bg-light h-px opacity-[.15]"></div>
              </li>
              <li className="relative px-[32px] py-[23px]">
                <Link
                  href="/column"
                  className="text-light font-light text-lg hover:opacity-70"
                >
                  コラム一覧
                </Link>
                <div className="w-full absolute left-0 bottom-0 bg-light h-px opacity-[.15]"></div>
              </li>
              <li className="relative px-[32px] py-[23px]">
                <Link
                  href="/"
                  className="text-light font-light text-lg hover:opacity-70"
                >
                  設定
                </Link>
                <div className="w-full absolute left-0 bottom-0 bg-light h-px opacity-[.15]"></div>
              </li>
            </ul>
          </nav>
        </div>
      ) : null}
    </>
  );
};

export default Header;
