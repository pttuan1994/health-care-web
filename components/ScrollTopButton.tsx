import { useEffect, useRef } from "react";
import styled from "@emotion/styled";

const ButtonShape = styled.button`
  display: none;
  position: fixed;
  bottom: 272px;
  right: 80px;
  width: 48px;
  height: 48px;
  background-color: ${({ theme }: any) => theme.colors.light};
`;

const ScrollTopButton = () => {
  const buttonRef = useRef<any>();
  // When the user clicks on the button, scroll to the top of the document
  const topFunction = () => {
    document.body.scrollIntoView({ behavior: "smooth" });
  };
  useEffect(() => {
    const scrollFunction = () => {
      if (
        document.body.scrollTop > 20 ||
        document.documentElement.scrollTop > 20
      ) {
        buttonRef.current.style.display = "block";
      } else {
        buttonRef.current.style.display = "none";
      }
    };
    window.addEventListener("scroll", scrollFunction);
    return function cleanup() {
      window.removeEventListener("scroll", scrollFunction);
    };
  }, []);

  return (
    <ButtonShape
      ref={buttonRef}
      className="cursor-pointer hover:opacity-70 rounded-full"
      onClick={topFunction}
    >
      <img
        className="object-cover w-full h-full"
        src="/icons/icon_scroll.png"
        alt="icon top"
      />
    </ButtonShape>
  );
};

export default ScrollTopButton;
