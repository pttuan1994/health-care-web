const Footer = () => {
  return (
    <div className="z-10 bg-dark500 py-[56px]">
      <ul className="container flex items-center justify-start space-x-10">
        <li className="text-light font-light text-[11px]">会員登録</li>
        <li className="text-light font-light text-[11px]">運営会社</li>
        <li className="text-light font-light text-[11px]">利用規約</li>
        <li className="text-light font-light text-[11px]">
          個人情報の取扱について
        </li>
        <li className="text-light font-light text-[11px]">
          特定商取引法に基づく表記
        </li>
        <li className="text-light font-light text-[11px]">お問い合わせ</li>
      </ul>
    </div>
  );
};

export default Footer;
