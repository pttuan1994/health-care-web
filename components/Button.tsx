import styled from "@emotion/styled";

const ButtonShape = styled.button`
  background: ${({ theme }: any) =>
    `linear-gradient(32.95deg, ${theme.colors.primary300} 8.75%, ${theme.colors.primary400} 86.64%)`};
`;

const Button = ({ text, onClick }) => {
  return (
    <ButtonShape
      className="cursor-pointer hover:opacity-70 min-w-[296px] py-3.5 px-8 rounded"
      onClick={onClick}
    >
      <p className="text-light font-light text-lg">{text}</p>
    </ButtonShape>
  );
};

export default Button;
