import { useReducer, createContext, useContext } from "react";
import { produce } from "immer";
import { useRouter } from "next/router";
import { faker } from "@faker-js/faker";
import NProgress from "nprogress";

// Mockup data

const dataSection1 = [
  [...Array(12)].map(() => faker.datatype.number({ min: 0, max: 100 })),
  [...Array(12)].map(() => faker.datatype.number({ min: 0, max: 100 })),
];

const dataSection2 = [
  {
    text: "Morning",
    icon: "/icons/icon_knife.png",
    link: "/",
  },
  {
    text: "Lunch",
    icon: "/icons/icon_knife.png",
    link: "/",
  },
  {
    text: "Dinner",
    icon: "/icons/icon_knife.png",
    link: "/",
  },
  {
    text: "Snack",
    icon: "/icons/icon_cup.png",
    link: "/",
  },
];

const dataSection3 = [
  {
    image: "/images/m01.jpg",
    text: "05.21.Morning",
  },
  {
    image: "/images/l03.jpg",
    text: "05.21.Lunch",
  },
  {
    image: "/images/d01.jpg",
    text: "05.21.Dinner",
  },
  {
    image: "/images/l01.jpg",
    text: "05.21.Snack",
  },
  {
    image: "/images/m01.jpg",
    text: "05.20.Morning",
  },
  {
    image: "/images/l02.jpg",
    text: "05.20.Lunch",
  },
  {
    image: "/images/d02.jpg",
    text: "05.20.Dinner",
  },
  {
    image: "/images/s01.jpg",
    text: "05.20.Snack",
  },
];

export const HOME_ACTION = {
  SET_DATA: "SET_DATA",
  SET_RECORD: "SET_RECORD",
};

const initialState = {
  data: {},
  records: [],
};

const reducer = produce((draft: typeof initialState, action) => {
  switch (action.type) {
    case HOME_ACTION.SET_DATA: {
      draft.data = action.payload;
      break;
    }
    case HOME_ACTION.SET_RECORD: {
      draft.records = draft.records.concat(action.payload);
      break;
    }
    default:
      break;
  }
});

const HomeContext = createContext({
  data: {} as any,
  records: [] as any,
  getData: () => {},
  getRecord: () => {},
});

const HomeStore = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const router = useRouter();

  // Fake get mockup data from BE
  const getData = async () => {
    NProgress.start();
    dispatch({
      type: HOME_ACTION.SET_DATA,
      payload: {
        section1: dataSection1,
        section2: dataSection2,
        percent: 75,
      },
    });
    setTimeout(() => {
      NProgress.done();
    }, 500);
  };
  const getRecord = async () => {
    NProgress.start();
    dispatch({
      type: HOME_ACTION.SET_RECORD,
      payload: dataSection3,
    });
    setTimeout(() => {
      NProgress.done();
    }, 500);
  };

  return (
    <HomeContext.Provider
      value={{
        ...state,
        getData,
        getRecord,
      }}
    >
      {children}
    </HomeContext.Provider>
  );
};

export const useHome = () => useContext(HomeContext);

export default HomeStore;
