import { useReducer, createContext, useContext } from "react";
import { produce } from "immer";
import { useRouter } from "next/router";
import { faker } from "@faker-js/faker";
import NProgress from "nprogress";

// Mockup data

const dataSection1 = [
  {
    image: "/images/MyRecommend-1.jpg",
    title: "BODY RECORD",
    subTitle: "自分のカラダの記録",
    action: "/",
  },
  {
    image: "/images/MyRecommend-2.jpg",
    title: "MY EXERCISE",
    subTitle: "自分の運動の記録",
    action: "/",
  },
  {
    image: "/images/MyRecommend-3.jpg",
    title: "MY DIARY",
    subTitle: "自分の日記",
    action: "/",
  },
];

const dataSection2 = [
  [...Array(12)].map(() => faker.datatype.number({ min: 0, max: 100 })),
  [...Array(12)].map(() => faker.datatype.number({ min: 0, max: 100 })),
];

const dataSection3 = [...Array(12)].map(() => ({
  name: "家事全般（立位・軽い）",
  time: "10 min",
  calo: "26kcal",
}));

const dataSection4 = [...Array(8)].map(() => ({
  date: "2021.05.21",
  time: "23:25",
  title: "私の日記の記録が一部表示されます。",
  description:
    "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…",
}));

export const RECORD_ACTION = {
  SET_DATA: "SET_DATA",
  SET_DIARY: "SET_DIARY",
};

const initialState = {
  data: {},
  diary: [],
};

const reducer = produce((draft: typeof initialState, action) => {
  switch (action.type) {
    case RECORD_ACTION.SET_DATA: {
      draft.data = action.payload;
      break;
    }
    case RECORD_ACTION.SET_DIARY: {
      draft.diary = draft.diary.concat(action.payload);
      break;
    }
    default:
      break;
  }
});

const RecordContext = createContext({
  data: {} as any,
  diary: [] as any,
  getData: () => {},
  getDiary: () => {},
});

const RecordStore = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const router = useRouter();

  // Fake get mockup data from BE
  const getData = async () => {
    NProgress.start();
    dispatch({
      type: RECORD_ACTION.SET_DATA,
      payload: {
        section1: dataSection1,
        section2: dataSection2,
        section3: dataSection3,
      },
    });
    setTimeout(() => {
      NProgress.done();
    }, 500);
  };
  const getDiary = async () => {
    NProgress.start();
    dispatch({
      type: RECORD_ACTION.SET_DIARY,
      payload: dataSection4,
    });
    setTimeout(() => {
      NProgress.done();
    }, 500);
  };

  return (
    <RecordContext.Provider
      value={{
        ...state,
        getData,
        getDiary,
      }}
    >
      {children}
    </RecordContext.Provider>
  );
};

export const useRecord = () => useContext(RecordContext);

export default RecordStore;
