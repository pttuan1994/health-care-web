import React, { createContext } from "react";
import HomeStore from "./home";
import RecordStore from "./record";
import ColumnStore from "./column";

const StoreContext = createContext({});

const StoreProvider = ({ children }) => {
  return (
    <StoreContext.Provider value={{}}>
      <HomeStore>
        <RecordStore>
          <ColumnStore>{children}</ColumnStore>
        </RecordStore>
      </HomeStore>
    </StoreContext.Provider>
  );
};

export default StoreProvider;
