import { useReducer, createContext, useContext } from "react";
import { produce } from "immer";
import { useRouter } from "next/router";
import NProgress from "nprogress";

// Mockup data

const dataSection1 = [
  {
    title: "RECOMMENDED COLUMN",
    subTitle: "オススメ",
    action: "/",
  },
  {
    title: "RECOMMENDED DIET",
    subTitle: "ダイエット",
    action: "/",
  },
  {
    title: "RECOMMENDED BEAUTY",
    subTitle: "美容",
    action: "/",
  },
  {
    title: "RECOMMENDED HEALTH",
    subTitle: "健康",
    action: "/",
  },
];

const dataSection2 = [...Array(8)].map((item, index) => {
  return {
    date: "2021.05.17",
    time: "23:25",
    image: `/images/column-${index + 1}.jpg`,
    title: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
    tags: ["魚料理", "和食", "DHA"],
  };
});

export const COLUMN_ACTION = {
  SET_DATA: "SET_DATA",
  SET_COLUMN: "SET_COLUMN",
};

const initialState = {
  data: {},
  column: [],
};

const reducer = produce((draft: typeof initialState, action) => {
  switch (action.type) {
    case COLUMN_ACTION.SET_DATA: {
      draft.data = action.payload;
      break;
    }
    case COLUMN_ACTION.SET_COLUMN: {
      draft.column = draft.column.concat(action.payload);
      break;
    }
    default:
      break;
  }
});

const ColumnContext = createContext({
  data: {} as any,
  column: [] as any,
  getData: () => {},
  getColumn: () => {},
});

const ColumnStore = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const router = useRouter();

  // Fake get mockup data from BE
  const getData = async () => {
    NProgress.start();
    dispatch({
      type: COLUMN_ACTION.SET_DATA,
      payload: {
        section1: dataSection1,
      },
    });
    setTimeout(() => {
      NProgress.done();
    }, 500);
  };
  const getColumn = async () => {
    NProgress.start();
    dispatch({
      type: COLUMN_ACTION.SET_COLUMN,
      payload: dataSection2,
    });
    setTimeout(() => {
      NProgress.done();
    }, 500);
  };

  return (
    <ColumnContext.Provider
      value={{
        ...state,
        getData,
        getColumn,
      }}
    >
      {children}
    </ColumnContext.Provider>
  );
};

export const useColumn = () => useContext(ColumnContext);

export default ColumnStore;
