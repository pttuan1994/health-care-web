import { useEffect } from "react";
import { useRouter } from "next/router";
import type { AppProps } from "next/app";
import { appWithTranslation } from "next-i18next";
import nextI18NextConfig from "@/next-i18next.config.js";
import ThemeProvider from "@/theme/styledGlobal";
import styled from "@emotion/styled";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
import "@/theme/tailwindGlobal.css";

import Header from "@/components/Header";
import Footer from "@/components/Footer";
import ScrollTopButton from "@/components/ScrollTopButton";

import StoreProvider from "@/store";

const Wrapper = styled.div`
  background-color: ${({ theme }: any) => theme.colors.background};
  min-height: calc(100vh - 129px);
  padding-top: 4rem;
  padding-bottom: 4rem;
`;

function App({ Component, pageProps }: AppProps) {
  const router = useRouter();

  useEffect(() => {
    const handleRouteChange = (url, { shallow }) => {
      console.log(
        `App is changing to ${url} ${
          shallow ? "with" : "without"
        } shallow routing`
      );
      NProgress.start();
    };

    const handleRouteChangeDone = (url, { shallow }) => {
      NProgress.done();
    };

    router.events.on("routeChangeStart", handleRouteChange);
    router.events.on("routeChangeComplete", handleRouteChangeDone);
    router.events.on("routeChangeError", handleRouteChangeDone);

    // If the component is unmounted, unsubscribe
    // from the event with the `off` method:
    return () => {
      router.events.off("routeChangeStart", handleRouteChange);
      router.events.off("routeChangeComplete", handleRouteChangeDone);
      router.events.off("routeChangeError", handleRouteChangeDone);
    };
  }, []);
  return (
    <StoreProvider>
      <ThemeProvider>
        <Header />
        <Wrapper>
          <Component {...pageProps} />
        </Wrapper>
        <Footer />
        <ScrollTopButton />
      </ThemeProvider>
    </StoreProvider>
  );
}

export default appWithTranslation(App, nextI18NextConfig);
