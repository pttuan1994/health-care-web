import { NextPage } from "next";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import styled from "@emotion/styled";
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";

import { theme as themeTailwind } from "../tailwind.config";

import { SEO } from "components/SEO";
import HexagonButton from "@/components/HexagonButton";
import Button from "@/components/Button";
import ChartLine from "@/components/ChartLine";

import "react-circular-progressbar/dist/styles.css";
import { useHome } from "@/store/home";
import { useEffect } from "react";

const theme: any = themeTailwind.extend;

const CircleStatus = styled(CircularProgressbar)`
  position: absolute;
  width: 181px;
  height: 181px;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
`;

const TextStatus = styled.div`
  display: flex;
  align-items: center;
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  text-shadow: 0px 0px 6px #fca500;
`;

const MainGraph = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: calc(100% - 540px);
`;

const Home: NextPage = () => {
  const { t } = useTranslation("home");
  const { getData, getRecord, data, records = [] } = useHome();
  const { percent = 0, section1 = [], section2 = [] } = data;
  useEffect(() => {
    getData();
    getRecord();
  }, []);
  return (
    <>
      <SEO title="Home" />
      <section className="flex items-center justify-start bg-dark600">
        <div className="relative w-[540px] h-[312px]">
          <img
            className="object-cover w-full h-full"
            src="/images/main_photo.png"
            alt="main photo"
          ></img>
          <CircleStatus
            strokeWidth={2}
            value={percent}
            styles={buildStyles({
              // Colors
              pathColor: theme.colors.light,
              trailColor: "rgba(0,0,0,0)",
            })}
          />
          <TextStatus className="space-x-2">
            <p className="font-inter text-light text-lg">05/21</p>
            <p className="font-inter text-light text-[25px]">{percent}%</p>
          </TextStatus>
        </div>
        <MainGraph>
          <ChartLine data={section1} width={589} height={294} />
        </MainGraph>
      </section>
      <section className="container flex items-center justify-around my-5 py-5">
        {section2.map((item, index) => (
          <HexagonButton
            key={index}
            text={item.text}
            icon={item.icon}
            onClick={() => {}}
          ></HexagonButton>
        ))}
      </section>
      <section className="container grid grid-cols-4 gap-4">
        {records.map((item, index) => (
          <div
            key={index}
            className="relative w-[234px] h-[234px] cursor-pointer hover:opacity-70"
          >
            <img
              className="object-cover w-full h-full"
              src={item.image}
              alt="top photo"
            ></img>
            <p className="absolute bottom-0 left-0 bg-primary300 p-2 font-inter text-light text-[15px]">
              {item.text}
            </p>
          </div>
        ))}
      </section>
      <section className="container text-center my-10">
        <Button
          text="記録をもっと見る"
          onClick={() => {
            getRecord();
          }}
        ></Button>
      </section>
    </>
  );
};

export const getStaticProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["home"])),
    },
  };
};

export default Home;
