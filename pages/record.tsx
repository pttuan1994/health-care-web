import { NextPage } from "next";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import styled from "@emotion/styled";

import { SEO } from "components/SEO";
import ChartLine from "@/components/ChartLine";
import Button from "@/components/Button";

import { useRecord } from "@/store/record";
import { useEffect } from "react";

const TableScroll = styled.div`
  height: 192px;
  width: 100%;
  overflow-y: scroll;
  &::-webkit-scrollbar-track {
    border-radius: 3px;
    background-color: ${({ theme }: any) => theme.colors.gray400};
  }

  &::-webkit-scrollbar {
    width: 6px;
    background-color: ${({ theme }: any) => theme.colors.dark500};
  }

  &::-webkit-scrollbar-thumb {
    border-radius: 3px;
    height: 34px;
    background-color: ${({ theme }: any) => theme.colors.primary300};
  }
`;

const Record: NextPage = () => {
  const { t } = useTranslation("record");
  const { getData, getDiary, data, diary = [] } = useRecord();
  const { section1 = [], section2 = [], section3 = [] } = data;
  useEffect(() => {
    getData();
    getDiary();
  }, []);
  return (
    <>
      <SEO title="Record" />
      <section className="container flex item-center justify-between my-[56px]">
        {section1.map((item, index) => (
          <div
            key={index}
            className="relative w-[288px] h-[288px] cursor-pointer hover:opacity-90 border-[24px] border-primary300"
            onClick={() => {}}
          >
            <div className="absolute w-full h-full mix-blend-luminosity">
              <img
                className="object-cover w-full h-full"
                src={item.image}
                alt="Recommend photo"
              ></img>
              <div className="absolute top-0 left-0 bg-black w-full h-full opacity-60"></div>
            </div>
            <div className="absolute w-full h-full flex flex-col items-center justify-center">
              <h3 className="font-inter text-[25px] text-primary300 mb-1.5">
                {item.title}
              </h3>
              <p className="text-light font-light text-center text-sm bg-primary400 min-w-[160px] py-0.5 px-2">
                {item.subTitle}
              </p>
            </div>
          </div>
        ))}
      </section>
      <section className="container">
        <div className="w-full bg-dark500">
          <div className="flex items-center justify-start">
            <p className="text-light font-inter text-[15px] py-4 px-6">
              BODY <br /> RECORD
            </p>
            <p className="text-light font-inter text-[22px]">2021.05.21</p>
          </div>
          <div className="w-full flex justify-center">
            <ChartLine data={section2} width={865} height={210} />
          </div>
          <div className="flex items-center justify-start space-x-3 py-4 px-6">
            <button className="bg-light rounded-[11px] w-[56px] font-light text-[15px] text-primary300">
              日
            </button>
            <button className="bg-light rounded-[11px] w-[56px] font-light text-[15px] text-primary300">
              週
            </button>
            <button className="bg-light rounded-[11px] w-[56px] font-light text-[15px] text-primary300">
              月
            </button>
            <button className="bg-primary300 rounded-[11px] w-[56px] font-light text-[15px] text-light">
              年
            </button>
          </div>
        </div>
      </section>
      <section className="container my-[56px]">
        <div className="w-full bg-dark500 pr-6 pb-6">
          <div className="flex items-center justify-start">
            <p className="text-light font-inter text-[15px] py-4 px-6">
              MY <br /> EXERCISE
            </p>
            <p className="text-light font-inter text-[22px]">2021.05.21</p>
          </div>
          <TableScroll>
            <ul className="grid grid-cols-2 gap-x-12 gap-y-4 px-6">
              {section3.map((item, index) => (
                <li
                  key={index}
                  className="border-b border-gray400 flex items-center justify-between"
                >
                  <div className="flex gap-2">
                    <p className="text-light">&#x2022;</p>
                    <div>
                      <p className="font-light text-light text-[15px]">
                        {item.name}
                      </p>
                      <p className="font-inter text-primary300 text-[15px]">
                        {item.calo}
                      </p>
                    </div>
                  </div>
                  <p className="font-inter text-primary300 text-lg">
                    {item.time}
                  </p>
                </li>
              ))}
            </ul>
          </TableScroll>
        </div>
      </section>
      <section className="container">
        <p className="font-inter text-[22px] text-dark500">MY DIARY</p>
        <div className="grid grid-cols-4 gap-6">
          {diary.map((item, index) => (
            <div
              key={index}
              className="bg-light border-2 border-gray300 p-4 w-[231px] h-[231px]"
            >
              <p className="font-inter text-lg text-dark500 leading-4">
                {item.date}
              </p>
              <p className="font-inter text-lg text-dark500 mb-2">
                {item.time}
              </p>
              <p className="font-light text-xs text-dark500">{item.title}</p>
              <p className="font-light text-xs text-dark500">
                {item.description}
              </p>
            </div>
          ))}
        </div>
      </section>
      <section className="container text-center my-10">
        <Button
          text="自分の日記をもっと見る"
          onClick={() => {
            getDiary();
          }}
        ></Button>
      </section>
    </>
  );
};

export const getStaticProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["record"])),
    },
  };
};

export default Record;
