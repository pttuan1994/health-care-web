import { SEO } from "components/SEO";
import { NextPage } from "next";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import Button from "@/components/Button";
import { useColumn } from "@/store/column";
import { useEffect } from "react";

const Column: NextPage = () => {
  const { t } = useTranslation("column");
  const { getData, getColumn, data, column = [] } = useColumn();
  const { section1 = [] } = data;
  useEffect(() => {
    getData();
    getColumn();
  }, []);
  return (
    <>
      <SEO title="Record" />
      <section className="container flex items-center justify-between my-[56px]">
        {section1.map((item, index) => (
          <div
            key={index}
            className="w-[216px] bg-dark600 p-6 text-center flex flex-col items-center justify-center cursor-pointer hover:opacity-90"
            onClick={() => {}}
          >
            <p className="font-inter text-[22px] text-primary300">
              {item.title}
            </p>
            <div className="w-[56px] h-px bg-light my-1"></div>
            <p className="font-light text-lg text-light">{item.subTitle}</p>
          </div>
        ))}
      </section>
      <section className="container grid grid-cols-4 gap-x-4 gap-y-8">
        {column.map((item, index) => (
          <div
            key={index}
            className="w-[234px] cursor-pointer hover:opacity-90"
            onClick={() => {}}
          >
            <div className="relative w-[234px] h-[144px]">
              <img
                className="object-cover w-full h-full"
                src={item.image}
                alt="column photo"
              ></img>
              <span className="w-[144px] absolute bottom-0 left-0 bg-primary300 py-px px-2 font-inter text-light text-[15px] flex items-center justify-between">
                <p>{item.date}</p>
                <p>{item.time}</p>
              </span>
            </div>
            <p className="font-light text-[15px] text-dark500 py-[6px]">
              {item.title}
            </p>
            <div className="flex items-center justify-start space-x-2">
              {item.tags.map((tag, indexTag) => (
                <p
                  key={indexTag}
                  className="font-light text-xs text-primary400"
                >
                  #{tag}
                </p>
              ))}
            </div>
          </div>
        ))}
      </section>
      <section className="container text-center my-10">
        <Button
          text="コラムをもっと見る"
          onClick={() => {
            getColumn();
          }}
        ></Button>
      </section>
    </>
  );
};

export const getStaticProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["column"])),
    },
  };
};

export default Column;
