This is a health care project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Health care Gitlab repository](https://gitlab.com/pttuan1994/health-care-web) - your feedback and contributions are welcome!

## Features

- [react-circular-progressbar] - To handle circle processbar in Home page.
- [react-chartjs-2] - To draw line charts in project.
- [@emotion/react] - To implement styled-component.
- [tailwindcss]- To handle style sheet in project.

## Thanks you!!!
